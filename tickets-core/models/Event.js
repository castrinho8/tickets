import Waterline from 'waterline'

const Event = Waterline.Collection.extend({

  identity: 'event',
  connection: 'myLocalMongo',

  attributes: {
    id: {
      type: 'string',
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    name: {
      type: 'string',
      notNull: true,
      required: true
    },
    description: {
      type: 'string',
      notNull: true,
      required: true
    },
    date: {
      type: 'string',
      required: true
    },
    location: {
      type: 'string',
      required: true
    },
    contactEmail: {
      type: 'email',
      email: true
    }
  }
})

module.exports = Event
