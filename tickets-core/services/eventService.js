import { app } from '../app'

const EventService = {

  adaptEventDataToDB (newEvent) {
    const eventToAdd = newEvent
    delete eventToAdd.id
    return eventToAdd
  },

  findAll () {
    return app.models.event.find({})
  },

  findById (id) {
    return app.models.event.findOne({ id })
  },

  create (newEvent) {
    const eventToAdd = this.adaptEventDataToDB(newEvent)
    return app.models.event.create(eventToAdd)
  },

  update (id, newEvent) {
    return new Promise((resolve, reject) => {
      this.findById(id).then((event) => {
        if (event == null) {
          reject(`Event with id ${id} not found`)
        } else {
          const eventToAdd = this.adaptEventDataToDB(newEvent)
          resolve(app.models.event.update({ id }, eventToAdd))
        }
      }).fail((err) => {
        reject(err)
      })
    })
  },

  delete (id) {
    return new Promise((resolve, reject) => {
      this.findById(id).then((event) => {
        if (event == null) {
          reject(`Event with id ${id} not found`)
        } else {
          resolve(app.models.event.destroy({ id }))
        }
      }).fail((err) => {
        reject(err)
      })
    })
  }
}

export default EventService
