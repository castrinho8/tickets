import express from 'express'
import EventService from '../services/eventService'

const router = express.Router()

router.get('/', (req, res, next) => {
  EventService.findAll().then((models) => (
    res.json(models)
  )).fail((err) => (
    res.json({ err }, 500)
  ))
})

router.post('/', (req, res, next) => {
  EventService.create(req.body).then((model) => (
    res.json(model)
  )).fail((err) => (
    res.json({ err }, 500)
  ))
})

router.get('/:id', (req, res, next) => {
  EventService.findById(req.params.id).then((model) => (
    res.json(model)
  )).fail((err) => (
    res.json({ err }, 500)
  ))
})

router.put('/:id', (req, res, next) => {
  EventService.update(req.params.id, req.body).then((newModel) => (
    res.json(newModel)
  )).catch((err) => {
    res.json({ err }, 500)
  })
})

router.delete('/:id', (req, res, next) => {
  EventService.delete(req.params.id).then((value) => (
    res.json(200)
  )).catch((err) => (
    res.json({ err }, 500)
  ))
})

export default router
