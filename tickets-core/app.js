import express from 'express'
import path from 'path'
import 'serve-favicon'
import logger from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import Waterline from 'waterline'
import diskAdapter from 'sails-disk'
import mongoAdapter from 'sails-mongo'

import routes from './controllers/index'
import events from './controllers/events'

import Event from './models/Event'

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use('/', routes)
app.use('/event', events)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

// WATERLINE CONFIG
// Instantiate a new instance of the ORM
const orm = new Waterline()

// Build A Config Object
const config = {
  // Setup Adapters
  // Creates named adapters that have been required
  adapters: {
    default: diskAdapter,
    disk: diskAdapter,
    mongo: mongoAdapter
  },
  // Build Connections Config
  // Setup connections using the named adapter configs
  connections: {
    myLocalDisk: {
      adapter: 'disk'
    },
    myLocalMongo: {
      adapter: 'mongo',
      host: 'localhost',
      port: 27017,
      database: 'foobar'
    }
  },
  defaults: {
    migrate: 'alter'
  }
}

orm.loadCollection(Event)

module.exports.app = app
module.exports.orm = { waterline: orm, config }
