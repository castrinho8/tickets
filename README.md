# Tickets #

### Presentational and Container Components ###

Following Redux suggestions, presentational components and container components which work with data were separated. That is an interesting pattern because it simplifies many things like refactoring features in redux state.

### Generic list ###

I've created a generic list because at first i wanted to manage tickets too (finally, i hadn't enought time) so my idea was to use this generic element to manage things like pagination or columns in all lists with the same component.

### Dockerfiles and separate projects ###

Both projects were created separatly to make easier distribution and deploy. Two Dockerfiles were created and Bitbucket's respository was connected with DockerHub to allow automatic Docker builds.

#### API ####
[api.corp.vacmatch.com](http://api.corp.vacmatch.com/)

#### Web ####
[web.corp.vacmatch.com](http://web.corp.vacmatch.com/)
