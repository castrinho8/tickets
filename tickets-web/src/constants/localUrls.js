
export default {

  eventList () {
    return '/'
  },

  event (id) {
    return `event/${id}`
  }
}
