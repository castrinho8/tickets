const urlconf = require('./urlconf')

let instance

export default class URL {
  constructor (mode = 'dev') {
    if (!instance) {
      instance = this
      this.host = urlconf[mode].host
      this.port = urlconf[mode].port
    }

    return instance
  }

  baseUrl () {
    return `http://${this.host}:${this.port}`
  }

  fetchEvents () {
    return `${this.baseUrl()}/event/`
  }

  addEvent () {
    return `${this.baseUrl()}/event/`
  }

  editEvent (id) {
    return `${this.baseUrl()}/event/${id}`
  }

  deleteEvent (id) {
    return `${this.baseUrl()}/event/${id}`
  }

}
