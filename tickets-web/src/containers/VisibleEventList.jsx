import { connect } from 'react-redux'
import EventList from '../components/event/EventList'
import { addEvent, fetchEvents } from '../actions/eventActions'
import { toggleModal } from '../actions/eventFormActions'

function mapStateToProps (state) {
  return {
    events: state.events.list,
    error: state.eventForm.errorMessage,
    showModal: state.eventForm.showModal
  }
}

function mapDispatchToProps (dispatch) {
  return {
    toggleModal: () => {
      dispatch(toggleModal())
    },
    fetchEvents: () => {
      dispatch(fetchEvents())
    },
    handleFormSubmit: (newEvent, props) => {
      dispatch(addEvent(newEvent))
    }
  }
}

const VisibleEventList = connect(
  mapStateToProps,
  mapDispatchToProps
)(EventList)

export default VisibleEventList
