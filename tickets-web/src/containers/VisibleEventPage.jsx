import { connect } from 'react-redux'
import Event from '../components/event/Event'
import { fetchEvents, editEvent, deleteEvent } from '../actions/eventActions'
import { toggleModal } from '../actions/eventFormActions'

const filterById = (events, id) => {
  const result = events.filter((e) => (e.id === id))
  return result.length > 0 ? result[0] : null
}

const mapStateToProps = (state, props) => ({
  event: filterById(state.events.list, props.eventId),
  error: state.eventForm.errorMessage,
  showModal: state.eventForm.showModal
})

function mapDispatchToProps (dispatch) {
  return {
    fetchEvents: () => {
      dispatch(fetchEvents())
    },
    toggleModal: () => {
      dispatch(toggleModal())
    },
    handleFormSubmit: (newEvent, props) => {
      dispatch(editEvent(newEvent))
    },
    handleDelete: (eventId) => {
      dispatch(deleteEvent(eventId))
    }
  }
}

const VisibleEventPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Event)

export default VisibleEventPage
