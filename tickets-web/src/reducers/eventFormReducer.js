import {
  TOGGLE_MODAL,
  ADD_EVENT,
  ADD_EVENT_FAILURE,
  EDIT_EVENT,
  EDIT_EVENT_FAILURE,
  DELETE_EVENT,
  DELETE_EVENT_FAILURE,
  FETCH_EVENTS,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE
} from '../constants/actionTypes'

import initialState from './initialState'

export default function (state = initialState.eventForm, action) {
  switch (action.type) {
    case TOGGLE_MODAL: {
      return {
        ...state,
        showModal: !state.showModal
      }
    }
    case ADD_EVENT: {
      return {
        errorMessage: null,
        showModal: false
      }
    }
    case ADD_EVENT_FAILURE: {
      return {
        errorMessage: action.payload,
        showModal: true
      }
    }
    case EDIT_EVENT: {
      return {
        errorMessage: null,
        showModal: false
      }
    }
    case EDIT_EVENT_FAILURE: {
      return {
        errorMessage: action.payload,
        showModal: true
      }
    }
    case DELETE_EVENT: {
      return {
        errorMessage: null,
        showModal: false
      }
    }
    case DELETE_EVENT_FAILURE: {
      return {
        errorMessage: action.payload,
        showModal: true
      }
    }
    case FETCH_EVENTS: {
      return {
        errorMessage: null
      }
    }
    case FETCH_EVENTS_SUCCESS: {
      return {
        errorMessage: null
      }
    }
    case FETCH_EVENTS_FAILURE: {
      return {
        errorMessage: action.payload
      }
    }
    default:
      return state
  }
}
