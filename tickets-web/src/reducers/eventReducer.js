import {
  TOGGLE_MODAL,
  ADD_EVENT,
  ADD_EVENT_FAILURE,
  EDIT_EVENT,
  DELETE_EVENT,
  FETCH_EVENTS,
  FETCH_EVENTS_SUCCESS
} from '../constants/actionTypes'

import initialState from './initialState'

export default function (state = initialState.events, action) {
  switch (action.type) {
    case TOGGLE_MODAL: {
      return {
        ...state
      }
    }
    case ADD_EVENT: {
      return {
        list: [
          ...state.list, action.payload
        ]
      }
    }
    case ADD_EVENT_FAILURE: {
      return {
        list: [
          ...state.list
        ]
      }
    }
    case EDIT_EVENT: {
      const editedElement = action.payload.length > 0 ? action.payload[0] : {}
      return {
        list: [...state.list.filter((e) => (e.id !== editedElement.id)), editedElement]
      }
    }
    case DELETE_EVENT: {
      return {
        list: state.list.filter((e) => (e.id !== action.payload))
      }
    }
    case FETCH_EVENTS: {
      return {
        list: state.list
      }
    }
    case FETCH_EVENTS_SUCCESS: {
      return {
        list: action.payload
      }
    }
    default:
      return state
  }
}
