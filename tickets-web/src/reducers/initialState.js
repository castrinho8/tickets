export default {
  events: {
    list: []
  },
  eventForm: {
    errorMessage: null,
    showModal: false
  },
  pagination: {
    first: 0,
    last: 0,
    count: 0
  }
}
