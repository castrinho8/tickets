import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
import eventReducer from './eventReducer'
import eventFormReducer from './eventFormReducer'

const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  events: eventReducer,
  eventForm: eventFormReducer
})

export default rootReducer
