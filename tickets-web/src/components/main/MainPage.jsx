import React from 'react'
import VisibleEventList from '../../containers/VisibleEventList'

function MainPage () {
  return (
    <div>
      <h2 className="text-center">Welcome to Tickets</h2>
      <br />
      <div>
        <VisibleEventList />
      </div>
    </div>
  )
}

export default MainPage
