import React, { Component, PropTypes } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Modal, Button, Alert, FormGroup, ControlLabel, FormControl,
  ButtonToolbar, HelpBlock } from 'react-bootstrap'
import moment from 'moment'

class EventForm extends Component {

  constructor (props) {
    super(props)

    this.handleDelete = this.handleDelete.bind(this)
    this.renderField = this.renderField.bind(this)
  }

  getValidationState ({ pristine, active, error }) {
    if (pristine) {
      return 'success'
    }
    if (error) {
      if (active) {
        return 'warning'
      }
      return 'error'
    }
    return 'success'
  }

  handleDelete () {
    const { handleDelete, editElement } = this.props
    handleDelete(editElement.id)
  }

  renderField ({ name, placeholder, input, label, type,
    meta: { touched, pristine, error, valid, active } }) {
    return (
      <FormGroup validationState={this.getValidationState({ pristine, active, error })}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl
          {...input}
          ref={c => { this[`${name}`] = c }}
          type={type}
          placeholder={placeholder || name}
        />
      {touched && error && <HelpBlock>{error}</HelpBlock>}
      </FormGroup>
    )
  }

  render () {
    const { handleSubmit, handleFormSubmit, toggleModal, editElement,
      errorMessage, showModal } = this.props
    const modalTitle = editElement != null ? 'Edit event' : 'Add event'
    const buttonText = editElement != null ? 'Edit' : 'Add event'
    const deleteHtml = editElement != null ? (
      <Button
        bsStyle="danger" className="pull-right" onClick={this.handleDelete}
      >Delete</Button>
    ) : <span />
    const errorHtml = errorMessage != null ? (
      <Alert bsStyle="danger" >{errorMessage.message}</Alert>
    ) : <br />

    return (
      <div>
        <Button className="pull-right" onClick={toggleModal}>{buttonText}</Button>
        <Modal
          bsSize="large"
          show={showModal} onHide={toggleModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>{modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={handleSubmit(handleFormSubmit)}>
              <Field
                name="name"
                label="Name"
                placeholder="Name"
                component={this.renderField}
                type="text"
              / >
              <Field
                name="description"
                label="Description"
                placeholder="Description"
                component={this.renderField}
                type="text"
              / >
              <Field
                name="location"
                label="Location"
                placeholder="Location"
                component={this.renderField}
                type="text"
              / >
              <Field
                name="date"
                label="Date"
                placeholder="Date"
                component={this.renderField}
                type="text"
              / >
              <Field
                name="contactEmail"
                label="Contact email"
                placeholder="Contact email"
                component={this.renderField}
                type="text"
              / >
              {errorHtml}
              <ButtonToolbar>
                <Button bsStyle="primary" type="submit" >Submit</Button>
                <Button onClick={toggleModal}>Cancel</Button>
                {deleteHtml}
              </ButtonToolbar>
            </form>
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

EventForm.propTypes = {
  handleSubmit: PropTypes.func,
  handleFormSubmit: PropTypes.func,
  toggleModal: PropTypes.func,
  handleDelete: PropTypes.func,
  editElement: PropTypes.object,
  errorMessage: PropTypes.object,
  showModal: PropTypes.bool
}

function isValidDate (date) {
  return moment(date, 'dd/mm/yyyy hh:mm').isValid()
}

function isValidEmail (email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

function validate ({ name, description, location, date, contactEmail }) {
  const errors = {}
  if (!name) {
    errors.name = 'Required'
  }
  if (!description) {
    errors.description = 'Required'
  }
  if (!location) {
    errors.location = 'Required'
  }
  if (!date) {
    errors.date = 'Required'
  } else if (!isValidDate(date)) {
    errors.date = 'Invalid date format'
  }
  if ((contactEmail != null) && (contactEmail !== '') && (!isValidEmail(contactEmail))) {
    errors.contactEmail = 'Invalid email format'
  }
  return errors
}

const formOptions = {
  form: 'event-form',
  validate
}

// Decorate the form component
export default reduxForm(formOptions)(EventForm)
