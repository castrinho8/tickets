import React, { PropTypes, Component } from 'react'
import { Label, Button } from 'react-bootstrap'
import '../../styles/event-page.css'
import EventForm from './EventForm'

class Event extends Component {

  constructor (props) {
    super()
  }

  render () {
    const { event, handleFormSubmit, handleDelete, toggleModal, showModal, error } = this.props
    let eventToEdit = (event != null) ? event : {}
    const { name, description, location, date, contactEmail } = eventToEdit
    return (
      <div>
        <EventForm
          handleFormSubmit={handleFormSubmit}
          handleDelete={handleDelete}
          toggleModal={toggleModal}
          showModal={showModal}
          editElement={eventToEdit}
          errorMessage={error}
          initialValues={eventToEdit}
        />
        <div className="element-container">
          <h2 className="event-title">{name}</h2>
          <h4><Label bsStyle="primary"><span>{location}</span> - <span>{date}</span></Label></h4>
          <hr />
          <p className="event-content">
            {description}
          </p>
          <p><Label>Contact</Label></p>
          <p><b>{contactEmail}</b></p>
          <p><Button bsStyle="success">Buy a ticket</Button></p>
        </div>
      </div>
    )
  }
}

Event.propTypes = {
  event: PropTypes.object,
  error: PropTypes.object,
  showModal: PropTypes.bool,
  handleFormSubmit: PropTypes.func,
  handleDelete: PropTypes.func,
  toggleModal: PropTypes.func,
  fetchEvents: PropTypes.func
}

module.exports = Event
