import React, { Component, PropTypes } from 'react'
import List from '../common/List'
import EventItem from './EventItem'
import EventForm from './EventForm'

class EventList extends Component {

  constructor (props) {
    super()
    props.fetchEvents()
  }

  getEvents () {
    const { events } = this.props
    return events.map((item) => <EventItem params={item} />)
  }

  render () {
    const { handleFormSubmit, error, showModal, toggleModal } = this.props
    return (
      <div>
        <EventForm
          handleFormSubmit={handleFormSubmit}
          errorMessage={error}
          showModal={showModal}
          toggleModal={toggleModal}
        />
        <List
          elements={this.getEvents()}
        />
      </div>
    )
  }
}

EventList.propTypes = {
  events: PropTypes.array,
  error: PropTypes.object,
  showModal: PropTypes.bool,
  handleFormSubmit: PropTypes.func,
  toggleModal: PropTypes.func,
  fetchEvents: PropTypes.func
}

export default EventList
