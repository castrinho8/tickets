import React, { PropTypes } from 'react'
import { Label, Button } from 'react-bootstrap'
import { Link } from 'react-router'
import localUrls from '../../constants/localUrls'

function EventItem (props) {
  const { id, name, description, location, date } = props.params
  const descriptionHtml = description.length > 100 ? `${description.slice(0, 100)}...` : description
  return (
    <div key={`eventItem-${id}`}>
      <h4><b>{name}</b></h4>
      <p><Label bsStyle="primary">{location}</Label> <small>{date}</small></p>
      <p>{descriptionHtml}</p>
      <Link to={localUrls.event(id)}>
        <Button bsStyle="success" bsSize="small">Show event</Button>
      </Link>
    </div>
  )
}

EventItem.propTypes = {
  params: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    location: PropTypes.string,
    date: PropTypes.any,
    contactEmail: PropTypes.string
  })
}

module.exports = EventItem
