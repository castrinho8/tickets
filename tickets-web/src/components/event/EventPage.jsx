import React, { PropTypes } from 'react'
import VisibleEventPage from '../../containers/VisibleEventPage'

function EventPage (props) {
  const { eventId } = props.params
  return (
    <div>
      <VisibleEventPage eventId={eventId} />
    </div>
  )
}

EventPage.propTypes = {
  params: PropTypes.shape({
    eventId: PropTypes.string
  })
}

export default EventPage
