import {
  TOGGLE_MODAL
} from '../constants/actionTypes'

export function toggleModal () {
  return {
    type: TOGGLE_MODAL
  }
}
