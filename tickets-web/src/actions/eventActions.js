import axios from 'axios'
import { reset } from 'redux-form'
import { hashHistory } from 'react-router'
import localUrls from '../constants/localUrls'

import {
  ADD_EVENT,
  ADD_EVENT_FAILURE,
  EDIT_EVENT,
  EDIT_EVENT_FAILURE,
  DELETE_EVENT,
  DELETE_EVENT_FAILURE,
  FETCH_EVENTS,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE
} from '../constants/actionTypes'

import URL from '../constants/urls.js'

const URLs = new URL()

export function fetchEvents () {
  return dispatch => {
    dispatch({
      type: FETCH_EVENTS
    })

    axios.get(URLs.fetchEvents())
      .then(response => {
        if (response.status === 200) {
          dispatch({
            type: FETCH_EVENTS_SUCCESS,
            payload: response.data
          })
        } else {
          dispatch({
            type: FETCH_EVENTS_FAILURE,
            payload: response.data
          })
        }
      })
      .catch(error => {
        dispatch({
          type: FETCH_EVENTS_FAILURE,
          payload: error
        })
      })
  }
}

export function addEvent (event) {
  return dispatch => {
    axios.post(URLs.addEvent(), event)
      .then(response => {
        if (response.status === 200) {
          dispatch({
            type: ADD_EVENT,
            payload: response.data
          })
          dispatch(reset('event-form'))
        } else {
          dispatch({
            type: ADD_EVENT_FAILURE,
            payload: response.data
          })
        }
      })
      .catch(error => {
        dispatch({
          type: ADD_EVENT_FAILURE,
          payload: error
        })
      })
  }
}

export function editEvent (event) {
  return dispatch => {
    axios.put(URLs.editEvent(event.id), event)
      .then(response => {
        if (response.status === 200) {
          dispatch({
            type: EDIT_EVENT,
            payload: response.data
          })
        } else {
          dispatch({
            type: EDIT_EVENT_FAILURE,
            payload: response.data
          })
        }
      })
      .catch(error => {
        dispatch({
          type: EDIT_EVENT_FAILURE,
          payload: error
        })
      })
  }
}

export function deleteEvent (eventId) {
  return dispatch => {
    axios.delete(URLs.deleteEvent(eventId), eventId)
      .then(response => {
        if (response.status === 200) {
          dispatch({
            type: DELETE_EVENT,
            payload: eventId
          })
          hashHistory.push(localUrls.eventList())
        } else {
          dispatch({
            type: DELETE_EVENT_FAILURE,
            payload: response.data
          })
        }
      })
      .catch(error => {
        dispatch({
          type: DELETE_EVENT_FAILURE,
          payload: error
        })
      })
  }
}
